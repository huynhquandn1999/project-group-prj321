<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Shiny Pet - Admin Page</title>
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="/ProjectPRJ321Demo/css/admin-page/sider.css">
        <link rel="stylesheet" href="/ProjectPRJ321Demo/css/admin-page/content.css">
        <link rel="stylesheet" href="/ProjectPRJ321Demo/css/public-page/snackbar.css">
        <script src="/ProjectPRJ321Demo/js/bootstrap/jquery-1.9.1.min.js"></script>
        <script src="/ProjectPRJ321Demo/js/bootstrap/popper.min.js"></script>
        <script src="/ProjectPRJ321Demo/js/bootstrap/bootstrap.js"></script>
    </head>

    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h5>Welcome, <s:property value="%{#session.FULLNAME}"/></h5>
                    <a href="/ProjectPRJ321Demo/logout" class="btn btn-danger" role="button">Logout</a>
                </div>

                <ul class="list-unstyled components">
                    <li>
                        <a href="/ProjectPRJ321Demo/loadDashboard">Dashboard</a>
                    </li>



                    <li>
                        <a href="#manage-service" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Service</a>
                        <ul class="collapse list-unstyled" id="manage-service">
                            <li>
                                <a href="/ProjectPRJ321Demo/adminViewListService?page=1">View list service</a>
                            </li>

                        </ul>
                    </li>

                    
                    <li>
                        <a href="#view-invoice" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">View Invoice</a>
                        <ul class="collapse list-unstyled" id="view-invoice">
                            
                            <li>
                                <a href="/ProjectPRJ321Demo/viewListServiceInvoice">Service invoices</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>

            <!-- Page Content  -->
            <div id="content">
                <div class="main-content">