<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="sider.jsp" %>
<link rel="stylesheet" href="/ProjectPRJ321Demo/css/member-page/edit-profile.css">
<div id="snackbar">
    <s:property value="%{mess}" />
</div>
<div class="title mb-4">View Profile</div>

<div class="container-fluid">
    <form action="/ProjectPRJ321Demo/updateProfile" method="POST">
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" name="username" id="username" class="form-control" value="<s:property value='%{dto.username}'/>"
                   readonly>
        </div>

        <div class="form-group">
            <label for="fullname">Fullname:</label>
            <input type="text" name="fullname" id="fullname" class="form-control" value="<s:property value='%{dto.fullname}'/>"
                   required maxlength="50">
        </div>

        <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" name="address" id="address" class="form-control" value="<s:property value='%{dto.address}'/>"
                   required>
        </div>

        <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="number" name="phone" id="phone" class="form-control" min="0" value="<s:property value='%{dto.phone}'/>"
                   required oninput="checkPhone('phone')">
        </div>

        <label>Gender:</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="gender" id="rdMale" value="Male" 
                   required <s:if test="%{dto.gender}">checked</s:if> >
            <label class="form-check-label" for="rdMale">Male</label>
        </div>

        <div class="form-check">
            <input class="form-check-input" type="radio" name="gender" id="rdFemale" value="Female" 
                   required <s:if test="%{!dto.gender}">checked</s:if> >
            <label class="form-check-label" for="rdFemale">Female</label>
        </div>

        
    </form>
    <script src="/ProjectPRJ321Demo/js/public-page/validate-signup.js"></script>
</div>
<%@include file="end-sider.jsp" %>
