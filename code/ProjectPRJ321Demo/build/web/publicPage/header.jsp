<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Shiny Pet</title>
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/public-page/header-footer.css">
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/public-page/suggestion.css">
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/public-page/snackbar.css">
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/public-page/index.css">
        <link rel="stylesheet" type="text/css" href="/ProjectPRJ321Demo/css/bootstrap/bootstrap.css">
        <script src="/ProjectPRJ321Demo/js/bootstrap/jquery-1.9.1.min.js"></script>
        <script src="/ProjectPRJ321Demo/js/bootstrap/popper.min.js"></script>
        <script src="/ProjectPRJ321Demo/js/bootstrap/bootstrap.js"></script>
        <script src="/ProjectPRJ321Demo/js/public-page/add-accessory-to-cart.js"></script>
    </head>

    <body>
        <header>
            <div class="row mx-5 my-4 text-center align-items-center">
                <!-- logo -->
                <div class="col-3">
                    <a href="/ProjectPRJ321Demo/"><img class="header-logo" src="/ProjectPRJ321Demo/img/header-footer/logo.png"></a>                    
                </div>

                <!-- search -->
                <div class="col-6" style="margin-top: 10px">
                    <div>
                        <div class="row">
                            <div class="form-group has-danger col-12 mx-0 px-0 autocomplete">
                                <input type="text" id="txtSearch-suggestion" name="txtSearch" class="form-control form-control-danger" placeholder="Search..."
                                       required minlength=3 oninput="searchAccessory()" onfocusin="searchAccessory()" onfocusout="clearSuggestion()"
                                       value="<s:property value="%{search}"/>">
                                <div class="autocomplete-items" id="suggestion">
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- login + cart -->
                <div class="col-3">
                    <div class="row">
                        <!-- login btn -->
                        <div class="col-8">
                            <s:if test="%{#session.USERNAME != null}">
                                <a href="/ProjectPRJ321Demo/loadProfile" class="nav-link">Hello, <s:property value="%{#session.FULLNAME}"/></a>
                                <a href="/ProjectPRJ321Demo/logout" class="btn btn-danger" role="button">Logout</a>
                            </s:if>
                            <s:else>
                                <a href="/ProjectPRJ321Demo/publicPage/login.jsp" class="btn btn-danger" role="button">Login</a>
                            </s:else>
                        </div>

                        <!-- cart -->
                        <div class="col-4 wrap-cart">
                            <a href="/ProjectPRJ321Demo/publicPage/viewCart.jsp" class="nav-link">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="icon-quantity"><span id="quantityCart">0</span></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <img src="/ProjectPRJ321Demo/img/header-footer/red-shopping-cart-hi.png" class="img-cart">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        
        <nav>
            <div class="row text-center align-items-center w-100">
                <div class="col-2"></div>
                
                <div class="col-2"><a href="/ProjectPRJ321Demo/loadListService?type=1" role="button" class="nav-link">SERVICE</a></div>
                <div class="col-2"></div>
            </div>
        </nav>
        <script src="/ProjectPRJ321Demo/js/public-page/suggestion.js"></script>