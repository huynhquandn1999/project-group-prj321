<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/publicPage/header.jsp" %>
<link rel="stylesheet" href="/ProjectPRJ321Demo/css/member-page/sider.css">

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <ul class="list-unstyled components">
                
                <li>
                    <a href="#manage-service" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Manage Pet</a>
                    <ul class="collapse list-unstyled" id="manage-service">
                        
                        <li>
                            <a href="/ProjectPRJ321Demo/loadCategoryType?locationPlace=pet">Add new pet</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#view-invoice" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">View Invoice</a>
                    <ul class="collapse list-unstyled" id="view-invoice">
                        
                        <li>
                            <a href="/ProjectPRJ321Demo/viewListServiceInvoice">Service invoices</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">
            <div class="main-content">
            