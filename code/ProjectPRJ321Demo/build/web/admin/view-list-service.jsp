<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/struts-tags" prefix="s" %>
<%@include file="home.jsp" %>
<link rel="stylesheet" href="/ProjectPRJ321Demo/css/admin-page/service.css">
<div class="title mb-4">View list service</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a role="button" class="btn btn-danger" id="deleteBtnAction" href="javascript(0);">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form action="adminViewListService" method="POST">
        <div class="row">
            <div class="col-10">
                
            </div>

            <div class="col-2">
                
            </div>
        </div>
    </form>
</div>

<s:if test="%{listService.isEmpty}">
    <div id="snackbar">Not found!</div>
</s:if>
<s:else>
    <table id="dtBasicExample" class="table table-striped table-bordered table-sm mt-4" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm text-center col-id">ID</th>
                <th class="th-sm text-center col-name">Name</th>
                <th class="th-sm text-center col-img">Image</th>
                <th class="th-sm text-center col-price">Price</th>
                <th class="th-sm text-center col-type">Type</th>
                <th class="th-sm text-center col-duration">Duration</th>
                <th class="th-sm text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <s:iterator value="listService">
                <tr>
                    <td class="align-middle col-id">
                        <s:property value="%{id}" />
                    </td>
                    <td class="align-middle col-name">
                        <s:property value="%{name}" />
                    </td>
                    <td class="align-middle text-center col-img">
                        <img class="img-accessory" src='/ProjectPRJ321Demo/img/file/service/<s:property value="%{image}"/>'>
                    </td>
                    <td class="align-middle text-center col-gender">
                        $<s:property value="%{price}" />
                    </td>
                    <td class="align-middle text-center col-status">
                        <s:property value="%{type.name}"/>
                    </td>
                    <td class="align-middle text-center col-status">
                        <s:if test="%{duration==0.5}">
                            0:30
                        </s:if>
                        <s:elseif test="%{duration==1}">
                            1:00
                        </s:elseif>
                        <s:elseif test="%{duration==1.5}">
                            1:30
                        </s:elseif>
                        <s:elseif test="%{duration==2}">
                            2:00
                        </s:elseif>
                    </td>
                    <td class="align-middle text-center">
                        <a href="/ProjectPRJ321Demo/adminViewDetailService?id=<s:property value="%{id}"/>" class="btn btn-sm btn-outline-info mb-2" role="button">
                        Detail
                        </a>
                        <br/>
                        
                    </td>
                </tr>
            </s:iterator>
        </tbody>
    </table>
    
</s:else>
<script src="/ProjectPRJ321Demo/js/admin-page/delete-action.js"></script>
<%@include file="end-sider.jsp" %>