/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dbs.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ServiceDTO;
import model.StaffDTO;
import model.TypeDTO;

/**
 *
 * @author Admin
 */
public class ServiceDAO {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
//    public int searchName(String search) throws SQLException, ClassNotFoundException {
//        int num = 0;
//        try {
//            conn = DBConnection.getConnection();
//
//            String sql = "SELECT count(ServiceID) as num FROM Servicess WHERE Name LIKE ? AND IsDelete = ?";
//
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, "%" + search + "%");
//            ps.setBoolean(2, false);
//
//            rs = ps.executeQuery();
//
//            if (rs.next()) {
//                num = rs.getInt("num");
//            }
//        } finally {
//            closeConnection();
//        }
//        return num;
//    }
    public List<ServiceDTO> searchName(String search) throws SQLException, ClassNotFoundException {
        List<ServiceDTO> result = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT ServiceID, Name, ForType, Duration, Price, Images FROM Servicess "
                    + "WHERE Name LIKE ? AND IsDelete = ? "
                    ;

            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setBoolean(2, false);

            rs = ps.executeQuery();

            int id;
            String name, image;
            float duration, price;
            TypeDTO type;
            ServiceDTO dto;

            result = new ArrayList<>();

            while (rs.next()) {
                id = rs.getInt("ServiceID");
                name = rs.getString("Name");
                type = (new TypeDAO()).findByID(rs.getInt("ForType"));
                duration = rs.getFloat("Duration");
                price = rs.getFloat("Price");
                image = rs.getString("Images");

                dto = new ServiceDTO(name, "", type, duration, price);
                dto.setId(id);
                dto.setImage(image);

                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    public ServiceDTO findByID(int id) throws SQLException, ClassNotFoundException {
        ServiceDTO dto = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT Name, ForType, Duration, Price, Descriptions, Images FROM Servicess WHERE ServiceID = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                String name = rs.getString("Name");
                TypeDTO type = (new TypeDAO()).findByID(rs.getInt("ForType"));
                float duration = rs.getFloat("Duration");
                float price = rs.getFloat("Price");
                String description = rs.getString("Descriptions");
                String image = rs.getString("Images");

                dto = new ServiceDTO(name, description, type, duration, price);
                dto.setImage(image);
                dto.setId(id);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
    public boolean delete(int id) throws SQLException, ClassNotFoundException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            conn.setAutoCommit(false);
            String sql = "UPDATE Servicess SET isDelete = ? WHERE serviceID = ?";
            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, true);
            ps.setInt(2, id);

            check = ps.executeUpdate() > 0;

            if (check) {
                sql = "UPDATE Staff_Service_Detail SET isDelete = ? WHERE serviceID = ?";

                ps = conn.prepareStatement(sql);
                ps.setBoolean(1, true);
                ps.setInt(2, id);

                check = ps.executeUpdate() >= 0;
                
                if (check) {
                    conn.commit();
                }
            }
        } finally {
            closeConnection();
        }
        return check;
    }
    public boolean update(ServiceDTO dto, List<StaffDTO> staff) throws SQLException, ClassNotFoundException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            conn.setAutoCommit(false);
            String sql = "UPDATE Servicess SET Name = ?, ForType = ?, Duration = ?, Price = ?, Descriptions = ? WHERE ServiceID = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getName());
            ps.setInt(2, dto.getType().getId());
            ps.setFloat(3, dto.getDuration());
            ps.setFloat(4, dto.getPrice());
            ps.setString(5, dto.getDescription());
            ps.setInt(6, dto.getId());

            check = ps.executeUpdate() > 0;

            if (check) {
                sql = "UPDATE Staff_Service_Detail SET IsDeleted = ? WHERE ServiceID = ?";
                ps = conn.prepareStatement(sql);

                ps.setBoolean(1, true);
                ps.setInt(2, dto.getId());

                check = ps.executeUpdate() > 0;

                
            }
        } finally {
            closeConnection();
        }
        return check;
    }
    public boolean updateImage(int id, String image) throws SQLException, ClassNotFoundException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            String sql = "UPDATE Servicess SET Images = ? WHERE ServiceID = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, image);
            ps.setInt(2, id);

            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
    public List<ServiceDTO> loadServiceToPage(int typeID) throws SQLException, ClassNotFoundException {
        List<ServiceDTO> result = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT ServiceID, Name, Duration, Price, Images, Descriptions "
                    + "FROM Servicess "
                    + "WHERE ForType = ? AND IsDelete = ? ";
            
            ps = conn.prepareStatement(sql);
            ps.setInt(1, typeID);
            ps.setBoolean(2, false);
            
            rs = ps.executeQuery();
            
            int serviceID;
            String name, image, description;
            float duration, price;
            ServiceDTO dto;
            result = new ArrayList<>();
            
            while (rs.next()) {
                serviceID = rs.getInt("ServiceID");
                name = rs.getString("Name");
                duration = rs.getFloat("Duration");
                price = rs.getFloat("Price");
                image = rs.getString("Images");
                description = rs.getString("Descriptions");
                
                dto = new ServiceDTO();
                dto.setId(serviceID);
                dto.setName(name);
                dto.setImage(image);
                dto.setDuration(duration);
                dto.setPrice(price);
                dto.setDescription(description);
                
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }

}
