/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dbs.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.StaffDTO;

/**
 *
 * @author Admin
 */
public class StaffDAO {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
    public int searchName(String search) throws SQLException, ClassNotFoundException {
        int num = 0;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT count(StaffID) as num FROM Staff WHERE StaffName LIKE ? AND IsDelete = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setBoolean(2, false);

            rs = ps.executeQuery();

            if (rs.next()) {
                num = rs.getInt("num");
            }
        } finally {
            closeConnection();
        }
        return num;
    }
    
    public int getTotalAvailableStaff() throws SQLException, ClassNotFoundException {
        int num = 0;
        try {
            conn = DBConnection.getConnection();
            
            String sql = "SELECT count(staffID) as num FROM Staff WHERE isAvailable = ? AND isDelete = ?";
            
            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, true);
            ps.setBoolean(2, false);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                num = rs.getInt("num");
            }
        } finally {
            closeConnection();
        }
        return num;
    }
    public List<StaffDTO> searchByLikeName(String search) throws SQLException, ClassNotFoundException {
        List<StaffDTO> result = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT StaffID, StaffName, Image, IsAvailable, Gender FROM Staff WHERE StaffName LIKE ? AND IsDelete = ?";

            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setBoolean(2, false);

            rs = ps.executeQuery();

            int id;
            String name, image;
            boolean available, gender;
            StaffDTO dto;
            result = new ArrayList<>();

            while (rs.next()) {
                id = rs.getInt("StaffID");
                name = rs.getString("StaffName");
                image = rs.getString("Image");
                available = rs.getBoolean("IsAvailable");
                gender = rs.getBoolean("Gender");

                dto = new StaffDTO(name, available, gender);
                dto.setImage(image);
                dto.setId(id);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    public StaffDTO findByID(int id) throws SQLException, ClassNotFoundException {
        StaffDTO dto = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT StaffName, Image, IsAvailable, Gender FROM Staff WHERE StaffID = ? AND IsDelete = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setBoolean(2, false);

            rs = ps.executeQuery();

            if (rs.next()) {
                String staffName = rs.getString("StaffName");
                String image = rs.getString("Image");
                boolean available = rs.getBoolean("IsAvailable");
                boolean gender = rs.getBoolean("Gender");

                dto = new StaffDTO(staffName, available, gender);
                dto.setImage(image);
                dto.setId(id);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
    public StaffDTO findByID(int id, boolean check) throws SQLException, ClassNotFoundException {
        StaffDTO dto = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT staffName, image, isAvailable, gender FROM Staff WHERE staffID = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            rs = ps.executeQuery();

            if (rs.next()) {
                String staffName = rs.getString("staffName");
                String image = rs.getString("image");
                boolean available = rs.getBoolean("isAvailable");
                boolean gender = rs.getBoolean("gender");

                dto = new StaffDTO(staffName, available, gender);
                dto.setImage(image);
                dto.setId(id);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
}
