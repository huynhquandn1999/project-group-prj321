/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dbs.DBConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.AccessoryDTO;
import model.CategoryDTO;
import model.TypeDTO;

/**
 *
 * @author Admin
 */
public class AccessoryDAO implements Serializable{
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private void closeConnection() throws SQLException {
        if (rs != null) rs.close();
        if (ps != null) ps.close();
        if (conn != null) conn.close();
    }
        public List<AccessoryDTO> loadTopMostBuy(int top, int typeID, int categoryID) throws SQLException, ClassNotFoundException {
        List<AccessoryDTO> result = null;
        try {
            conn = DBConnection.getConnection();
            
            String sql = "SELECT TOP " +top+ " AccessoryID, Name, Price, Image FROM Accessory "
                    + "WHERE ForType = ? AND CategoryID = ? AND IsDelete = ? "
                    ;
            
            ps = conn.prepareStatement(sql);
            ps.setInt(1, typeID);
            ps.setInt(2, categoryID);
            ps.setBoolean(3, false);
            
            rs = ps.executeQuery();
            
            String name, image;
            long id;
            float price;
            AccessoryDTO dto;
            
            result = new ArrayList<>();
            
            while (rs.next()) {
                name = rs.getString("name");
                image = rs.getString("image");
                id = rs.getLong("accessoryID");
                price = rs.getFloat("price");
                
                dto = new AccessoryDTO();
                dto.setName(name);
                dto.setImage(image);
                dto.setId(id);
                dto.setPrice(price);
                
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    
}
