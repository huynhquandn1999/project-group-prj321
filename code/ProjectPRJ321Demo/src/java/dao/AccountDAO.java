/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dbs.DBConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.AccountDTO;

/**
 *
 * @author Admin
 */
public class AccountDAO implements Serializable {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;
    
    private void closeConnection() throws SQLException {
        if (rs != null) rs.close();
        if (ps != null) ps.close();
        if (conn != null) conn.close();
    }
    public String[] checkLogin(String username, String password) throws SQLException, ClassNotFoundException {
        String[] result = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "Select Role, FullName From Account Where UserName = ? And Password = ? And IsDelete = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setBoolean(3, false);
            rs = ps.executeQuery();

            if (rs.next()) {
                result = new String[2];
                result[0] = rs.getString("Role");
                result[1] = rs.getString("FullName");
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public AccountDTO getAccountByID(String username) throws SQLException, ClassNotFoundException {
        AccountDTO dto = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT FullName, Address, Phone, Gender FROM Account WHERE UserName = ? AND IsDelete = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setBoolean(2, false);
            rs = ps.executeQuery();

            if (rs.next()) {
                String fullname = rs.getString("FullName");
                String address = rs.getString("Address");
                String phone = rs.getString("Phone");
                boolean gender = rs.getBoolean("Gender");

                dto = new AccountDTO(username, "", fullname, address, phone, "", false, gender);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
    public AccountDTO getAccountByID(String username, boolean check) throws SQLException, ClassNotFoundException {
        AccountDTO dto = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT fullname, address, phone, gender FROM Account WHERE username = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();

            if (rs.next()) {
                String fullname = rs.getString("fullname");
                String address = rs.getString("address");
                String phone = rs.getString("phone");
                boolean gender = rs.getBoolean("gender");

                dto = new AccountDTO(username, "", fullname, address, phone, "", false, gender);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        AccountDAO dao = new AccountDAO();
        String[] check = dao.checkLogin("tram", "123");
        System.out.println(check);
    }
}
