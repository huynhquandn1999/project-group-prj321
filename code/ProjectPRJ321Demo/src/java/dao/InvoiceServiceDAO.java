/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dbs.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.InvoiceServiceDTO;
import model.PetDTO;
import model.ServiceDTO;
import model.StaffDTO;

/**
 *
 * @author Admin
 */
public class InvoiceServiceDAO {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    public long getTotalInvoice() throws SQLException, ClassNotFoundException {
        long num = 0;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT count(InvoiceID) as num FROM Invoice_Service";
            
            ps = conn.prepareStatement(sql);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                num = rs.getLong("num");
            }
        } finally {
            closeConnection();
        }
        return num;
    }
    
    public long getTotalInvoice(int status) throws SQLException, ClassNotFoundException {
        long num = 0;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT count(InvoiceID) as num FROM Invoice_Service WHERE Status = ?";
            
            ps = conn.prepareStatement(sql);
            ps.setInt(1, status);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                num = rs.getLong("num");
            }
        } finally {
            closeConnection();
        }
        return num;
    }
    public boolean isPetFree(InvoiceServiceDTO dto) throws SQLException, ClassNotFoundException {
        boolean check = true;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT TimeStart as start, Duration\n"
                    + "FROM Invoice_Service\n"
                    + "WHERE DoingDate LIKE ? AND PetID = ? AND Status != ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + dto.getDoingDate() + "%");
            ps.setLong(2, dto.getPet().getId());
            ps.setInt(3, -1);

            rs = ps.executeQuery();

            float timeStart, timeEnd;
            while (rs.next()) {
                timeStart = rs.getFloat("start");
                timeEnd = timeStart + rs.getFloat("duration");

                if (timeEnd > dto.getTimeStart() || timeStart < (dto.getTimeStart() + dto.getService().getDuration())) {
                    check = false;
                }
            }
        } finally {
            closeConnection();
        }
        return check;
    }
    public List<InvoiceServiceDTO> getInvoiceByStaffAndDate(int staffID, String dateDoing) throws SQLException, ClassNotFoundException {
        List<InvoiceServiceDTO> result = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT InvoiceID, TimeStart, Duration, ServiceID as id "
                    + "FROM Invoice_Service "
                    + "WHERE StaffDoing = ? AND DoingDate LIKE ? AND Status = 1";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, staffID);
            ps.setString(2, "%" + dateDoing + "%");

            rs = ps.executeQuery();

            result = new ArrayList<>();

            InvoiceServiceDTO invoice;
            ServiceDTO service;

            while (rs.next()) {
                invoice = new InvoiceServiceDTO();
                invoice.setId(rs.getLong("InvoiceID"));
                invoice.setTimeStart(rs.getFloat("TimeStart"));

                service = new ServiceDTO();
                service.setId(rs.getInt("id"));
                service.setDuration(rs.getFloat("Duration"));

                invoice.setService(service);

                result.add(invoice);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public boolean insert(InvoiceServiceDTO dto) throws SQLException, ClassNotFoundException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();
            String sql = "INSERT INTO Invoice_Service(CreatedTime, Price, Status, DoingDate, PetID, TimeStart, ServiceID, Duration) VALUES(?,?,?,?,?,?,?,?)";

            ps = conn.prepareStatement(sql);
            ps.setTimestamp(1, dto.getCreateTime());
            ps.setFloat(2, dto.getPrice());
            ps.setInt(3, 0);
            ps.setString(4, dto.getDoingDate());
            ps.setLong(5, dto.getPet().getId());
            ps.setFloat(6, dto.getTimeStart());
            ps.setInt(7, dto.getService().getId());
            ps.setFloat(8, dto.getDuration());

            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
    
 
    public List<StaffDTO> listStaffAvailableForService(InvoiceServiceDTO dto) throws SQLException, ClassNotFoundException {
        List<StaffDTO> result = null;
        try {
            conn = DBConnection.getConnection();

            //1. Get all staff in serve this service
            String sql = "SELECT T.StaffID as id\n"
                    + "FROM Servicess S, Staff T, Staff_Service_Detail D\n"
                    + "WHERE S.ServiceID = D.ServiceID AND D.StaffID = T.StaffID AND D.IsDeleted = ? "
                    + "AND T.IsAvailable = ? AND S.IsDelete = ? AND D.IsDeleted = ? AND S.ServiceID = ?";

            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, false);
            ps.setBoolean(2, true);
            ps.setBoolean(3, false);
            ps.setBoolean(4, false);
            ps.setInt(5, dto.getService().getId());

            rs = ps.executeQuery();
            result = new ArrayList<>();
            StaffDAO staffDao = new StaffDAO();

            while (rs.next()) {
                result.add(staffDao.findByID(rs.getInt("id")));
            }
            closeConnection();

            HashMap<Integer, StaffDTO> map = new HashMap<>();
            for (StaffDTO staff : result) {
                map.put(staff.getId(), staff);
            }

            result = new ArrayList<>(map.values());

            //2. Get all invoice that a staff will do and check
            float timeEnd, timeStart;
            List<InvoiceServiceDTO> listInvoice;
            StaffDTO staffDTO;
            for (int i = 0; i < result.size(); i++) {
                staffDTO = result.get(i);

                listInvoice = this.getInvoiceByStaffAndDate(staffDTO.getId(), dto.getDoingDate().split("-")[0].trim());

                for (InvoiceServiceDTO invoiceServiceDTO : listInvoice) {
                    timeEnd = invoiceServiceDTO.getTimeStart() + invoiceServiceDTO.getService().getDuration();
                    timeStart = invoiceServiceDTO.getTimeStart();

                    if ((timeEnd > dto.getTimeStart() && timeEnd <= (dto.getTimeStart() + dto.getDuration()))
                            || ((timeStart < (dto.getTimeStart() + dto.getDuration())) && (timeStart >= dto.getTimeStart()))) {
                        result.set(i, null);
                    }
                }
            }

            while (result.remove(null)) {
            }

            //3. check waitting invoice and staff available
            conn = DBConnection.getConnection();
            sql = "SELECT count(InvoiceID) as num FROM Invoice_Service WHERE Status = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, 0);
            rs = ps.executeQuery();

            int numWaitting = 0;
            if (rs.next()) {
                numWaitting = rs.getInt("num");
            }

            if (numWaitting >= result.size()) {
                result.clear();
            }
        } finally {
            closeConnection();
        }
        return result;
    }
//    public long getListInvoiceOfMember(String username) throws SQLException, ClassNotFoundException {
//        long num = 0;
//        try {
//            conn = DBConnection.getConnection();
//
//            String sql = "SELECT count(InvoiceID) as num \n"
//                    + "FROM Invoice_Service I, Pet P\n"
//                    + "WHERE I.PetID = P.PetID AND P.OwnID = ?";
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, username);
//
//            rs = ps.executeQuery();
//
//            if (rs.next()) {
//                num = rs.getInt("num");
//            }
//        } finally {
//            closeConnection();
//        }
//        return num;
//    }
    public List<InvoiceServiceDTO> getListInvoiceOfMember(String username) throws SQLException, ClassNotFoundException {
        List<InvoiceServiceDTO> result = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT InvoiceID, I.Price as price, Status, DoingDate, TimeStart, "
                    + "I.PetID as pet, P.Name as petName, S.ServiceID as serviceID, S.Name as serviceName \n"
                    + "FROM Invoice_Service I, Pet P, Servicess S\n"
                    + "WHERE I.PetID = P.PetID AND P.OwnID = ? AND S.ServiceID = I.ServiceID\n"
                   ;

            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();

            long invoiceID;
            float price;
            int status;
            String doingDate;
            float timeStart;
            long petID;
            String petName;
            int serviceID;
            String serviceName;
            PetDTO petDto;
            ServiceDTO service;
            InvoiceServiceDTO dto;

            result = new ArrayList<>();

            while (rs.next()) {
                invoiceID = rs.getLong("InvoiceID");
                price = rs.getFloat("price");
                status = rs.getInt("Status");

                doingDate = rs.getString("DoingDate");
                timeStart = rs.getFloat("TimeStart");
                doingDate += " - " + checkTime(timeStart);

                petID = rs.getLong("pet");
                petName = rs.getString("petName");
                petDto = new PetDTO();
                petDto.setId(petID);
                petDto.setName(petName);

                serviceID = rs.getInt("serviceID");
                serviceName = rs.getString("serviceName");
                service = new ServiceDTO();
                service.setId(serviceID);
                service.setName(serviceName);

                dto = new InvoiceServiceDTO();
                dto.setId(invoiceID);
                dto.setPrice(price);
                dto.setStatus(status);
                dto.setDoingDate(doingDate);
                dto.setPet(petDto);
                dto.setService(service);

                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    private String checkTime(float timeStart) {
        String result;
        int floor = (int) Math.floor(timeStart);

        if (floor == timeStart) {
            result = floor + ":00";
        } else {
            result = floor + ":30";
        }
        return result;
    }
//    public long adminGetListServiceInvoice() throws SQLException, ClassNotFoundException {
//        long num = 0;
//        try {
//            conn = DBConnection.getConnection();
//
//            String sql = "SELECT count(InvoiceID) as num FROM Invoice_Service";
//            ps = conn.prepareStatement(sql);
//
//            rs = ps.executeQuery();
//
//            if (rs.next()) {
//                num = rs.getLong("num");
//            }
//        } finally {
//            closeConnection();
//        }
//        return num;
//    }
    public List<InvoiceServiceDTO> adminGetListServiceInvoice() throws SQLException, ClassNotFoundException {
        List<InvoiceServiceDTO> result = null;
        try {
            conn = DBConnection.getConnection();

            String sql = "SELECT InvoiceID, S.Name as serviceName, I.DoingDate as doingDate, I.Duration as duration, "
                    + "P.Name as petName, StaffDoing, Status, AdminConfirm, TimeStart\n"
                    + "FROM Invoice_Service I, Servicess S, Pet P\n"
                    + "WHERE I.ServiceID = S.ServiceID AND I.PetID = P.PetID "
                   ;

            ps = conn.prepareStatement(sql);

            rs = ps.executeQuery();

            long invoiceID;
            String serviceName, doingDate, petName, adminConfirm;
            int status, staffId;

            InvoiceServiceDTO dto;
            result = new ArrayList<>();

            while (rs.next()) {
                invoiceID = rs.getLong("InvoiceID");
                serviceName = rs.getString("serviceName");

                ServiceDTO service = new ServiceDTO();
                service.setName(serviceName);

                doingDate = rs.getString("DoingDate");
                doingDate += " - " + checkTime(rs.getFloat("TimeStart"));

                petName = rs.getString("petName");
                PetDTO pet = new PetDTO();
                pet.setName(petName);

                staffId = rs.getInt("StaffDoing");
                StaffDTO staff = (new StaffDAO()).findByID(staffId, true);

                status = rs.getInt("Status");
                adminConfirm = rs.getString("AdminConfirm");

                dto = new InvoiceServiceDTO(null, adminConfirm, doingDate, pet, staff, service, 0, 0, 0, status);
                dto.setId(invoiceID);

                result.add(dto);
            }

        } finally {
            closeConnection();
        }
        return result;
    }
    public InvoiceServiceDTO findById(long id) throws SQLException, ClassNotFoundException {
        InvoiceServiceDTO dto = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT CreatedTime, AdminConfirm, Price, Status, DoingDate, "
                    + "PetID, StaffDoing, TimeStart, ServiceID, Duration "
                    + "FROM Invoice_Service "
                    + "WHERE InvoiceID = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                Timestamp createTime = rs.getTimestamp("CreatedTime");
                String adminConfirm = rs.getString("AdminConfirm");
                float price = rs.getFloat("Price");
                int status = rs.getInt("Status");

                String doingDate = rs.getString("DoingDate");
                doingDate += " - " + checkTime(rs.getFloat("TimeStart"));

                PetDTO pet = (new PetDAO()).findByID(rs.getLong("PetID"));
                StaffDTO staff = (new StaffDAO()).findByID(rs.getInt("StaffDoing"), true);
                ServiceDTO service = (new ServiceDAO()).findByID(rs.getInt("ServiceID"));

                float duration = rs.getFloat("Duration");

                dto = new InvoiceServiceDTO(createTime, adminConfirm, doingDate, pet, staff, service, price, 0, duration, status);
                dto.setId(id);
                dto.setTimeStart(rs.getFloat("TimeStart"));
            }
        } finally {
            closeConnection();
        }
        return dto;
    }
    public List<StaffDTO> adminListStaffAvailableForService(InvoiceServiceDTO dto) throws SQLException, ClassNotFoundException {
        List<StaffDTO> result = null;
        try {
            conn = DBConnection.getConnection();
            String sql = "SELECT T.StaffID as id\n"
                    + "FROM Servicess S, Staff T, Staff_Service_Detail D\n"
                    + "WHERE S.ServiceID = D.ServiceID AND D.StaffID = T.StaffID AND D.IsDeleted = ? "
                    + "AND T.IsAvailable = ? AND S.IsDelete = ? AND D.IsDeleted = ? AND S.ServiceID = ?";

            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, false);
            ps.setBoolean(2, true);
            ps.setBoolean(3, false);
            ps.setBoolean(4, false);
            ps.setInt(5, dto.getService().getId());

            rs = ps.executeQuery();
            result = new ArrayList<>();
            StaffDAO staffDao = new StaffDAO();

            while (rs.next()) {
                result.add(staffDao.findByID(rs.getInt("id")));
            }
            closeConnection();

            HashMap<Integer, StaffDTO> map = new HashMap<>();
            for (StaffDTO staff : result) {
                map.put(staff.getId(), staff);
            }

            result = new ArrayList<>(map.values());

            float timeEnd, timeStart;
            List<InvoiceServiceDTO> listInvoice;
            StaffDTO staffDTO;
            System.out.println(result.size());
            for (int i = 0; i < result.size(); i++) {
                staffDTO = result.get(i);
                if (staffDTO == null) {
                    continue;
                }

                listInvoice = this.getInvoiceByStaffAndDate(staffDTO.getId(), dto.getDoingDate().split("-")[0].trim());

                for (InvoiceServiceDTO invoiceServiceDTO : listInvoice) {
                    timeEnd = invoiceServiceDTO.getTimeStart() + invoiceServiceDTO.getService().getDuration();
                    timeStart = invoiceServiceDTO.getTimeStart();
                           

                    if ((timeEnd > dto.getTimeStart() && timeEnd <= (dto.getTimeStart() + dto.getDuration()))
                            || ((timeStart < (dto.getTimeStart() + dto.getDuration())) && (timeStart >= dto.getTimeStart()))) {
                        result.set(i, null);
                    }
                }
            }

            while (result.remove(null)) {
            }

        } finally {
            closeConnection();
        }
        return result;
    }
    public boolean update(long id, int staff, int status, String adminConfirm) throws SQLException, ClassNotFoundException {
        boolean check = false;
        try {
            conn = DBConnection.getConnection();

            String sql = "UPDATE Invoice_Service SET StaffDoing = ?, Status = ?, AdminConfirm = ? WHERE InvoiceID = ?";
            ps = conn.prepareStatement(sql);
            if (staff > 0) {
                ps.setInt(1, staff);
            } else {
                ps.setNull(1, Types.INTEGER);
            }
            ps.setInt(2, status);
            ps.setLong(4, id);
            ps.setString(3, adminConfirm);

            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
}
