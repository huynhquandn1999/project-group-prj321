/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import dao.ServiceDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import model.ServiceDTO;

/**
 *
 * @author Admin
 */
public class AdminViewListServiceAction {
    private String name, mess;
    private int page, numOfPage;
    private List<ServiceDTO> listService;
    
    public AdminViewListServiceAction() {
        name = "";
        page = 1;
    }
    
    public String execute() {
        String label = "success";
        try {
            
            
            ServiceDAO dao = new ServiceDAO();
            
            
            listService = dao.searchName(name);
            
        } catch (Exception e) {
            
            mess = "Error";
        }
        return label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNumOfPage() {
        return numOfPage;
    }

    public void setNumOfPage(int numOfPage) {
        this.numOfPage = numOfPage;
    }

    public List<ServiceDTO> getListService() {
        return listService;
    }

    public void setListService(List<ServiceDTO> listService) {
        this.listService = listService;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }
}
