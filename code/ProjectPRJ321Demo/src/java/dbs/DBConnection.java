
package dbs;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection implements Serializable {
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection conn;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=PRJ321_Project_Demo1","hongquan1","hongquan1999");
        return conn;
    }
}
