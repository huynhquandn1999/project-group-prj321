/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guest;

import dao.AccessoryDAO;
import java.util.List;
import java.util.logging.Logger;
import model.AccessoryDTO;

/**
 *
 * @author Admin
 */
public class WelcomeAction {
    private String mess;
    private List<AccessoryDTO> listDogFood, listDogClothes;
            

    public WelcomeAction() {
    }

    public String execute() {
        try {
            AccessoryDAO dao = new AccessoryDAO();
            
            listDogFood = dao.loadTopMostBuy(5, 1, 1);
            listDogClothes = dao.loadTopMostBuy(5, 1, 2);
           
            
            
            
        } catch (Exception e) {
           
        }
        return "success";
    }
    
    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public List<AccessoryDTO> getListDogFood() {
        return listDogFood;
    }

    public void setListDogFood(List<AccessoryDTO> listDogFood) {
        this.listDogFood = listDogFood;
    }

    public List<AccessoryDTO> getListDogClothes() {
        return listDogClothes;
    }

    public void setListDogClothes(List<AccessoryDTO> listDogClothes) {
        this.listDogClothes = listDogClothes;
    }

    

    
}
