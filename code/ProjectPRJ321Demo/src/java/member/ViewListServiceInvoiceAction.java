/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package member;

import com.opensymphony.xwork2.ActionContext;
import dao.InvoiceServiceDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import model.InvoiceServiceDTO;

/**
 *
 * @author Admin
 */
public class ViewListServiceInvoiceAction {

    private String mess;

    private List<InvoiceServiceDTO> listServiceInvoice;

    private static final String ADMIN = "admin";
    private static final String MEMBER = "member";
    private static final String FAIL = "fail";

    public ViewListServiceInvoiceAction() {
        
    }

    public String execute() {
        String label = FAIL;
        try {
            Map<String, Object> session = ActionContext.getContext().getSession();

            String username = (String) session.get("USERNAME");
            String role = (String) session.get("ROLE");

            InvoiceServiceDAO dao = new InvoiceServiceDAO();

            if (role.equalsIgnoreCase("member")) {

                listServiceInvoice = dao.getListInvoiceOfMember(username);

                label = MEMBER;
            } else if (role.equalsIgnoreCase("admin")) {

                listServiceInvoice = dao.adminGetListServiceInvoice();

                label = ADMIN;
            }
        } catch (Exception e) {

            mess = "Error";
        }
        return label;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public List<InvoiceServiceDTO> getListServiceInvoice() {
        return listServiceInvoice;
    }

    public void setListServiceInvoice(List<InvoiceServiceDTO> listServiceInvoice) {
        this.listServiceInvoice = listServiceInvoice;
    }
}
