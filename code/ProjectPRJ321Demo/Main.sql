USE [master]
GO
/****** Object:  Database [PRJ321_Project_Demo1]    Script Date: 11/11/2019 3:28:23 PM ******/
CREATE DATABASE [PRJ321_Project_Demo1]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PRJ321_Project_Demo1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PRJ321_Project_Demo1.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PRJ321_Project_Demo1_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PRJ321_Project_Demo1_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PRJ321_Project_Demo1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ARITHABORT OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET  MULTI_USER 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PRJ321_Project_Demo1]
GO
/****** Object:  Table [dbo].[Accessory]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accessory](
	[AccessoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Brand] [nvarchar](20) NULL,
	[Price] [float] NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryID] [int] NULL,
	[Quantity] [int] NULL,
	[IsDelete] [bit] NULL,
	[ForType] [int] NULL,
	[Image] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[AccessoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](20) NULL,
	[FullName] [nvarchar](100) NULL,
	[Address] [nvarchar](max) NULL,
	[Phone] [varchar](10) NULL,
	[IsDelete] [bit] NULL,
	[Gender] [bit] NULL,
	[Role] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Accessory]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Accessory](
	[InvoiceID] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedTime] [datetime] NULL,
	[BuyerUserName] [nvarchar](20) NULL,
	[Status] [int] NULL,
	[AdminConfirm] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Accessory_Detail]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Accessory_Detail](
	[AccessoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[Quantity] [int] NULL,
	[SubPrice] [float] NULL,
	[InvoiceID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[AccessoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Service]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Invoice_Service](
	[InvoiceID] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedTime] [datetime] NULL,
	[AdminConfirm] [nvarchar](20) NULL,
	[Price] [float] NULL,
	[Status] [int] NULL,
	[DoingDate] [varchar](50) NULL,
	[PetID] [bigint] NULL,
	[StaffDoing] [int] NULL,
	[TimeStart] [float] NULL,
	[ServiceID] [bigint] NULL,
	[Duration] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pet]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pet](
	[PetID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[BirthYear] [int] NULL,
	[OwnID] [nvarchar](20) NULL,
	[TypeID] [int] NULL,
	[Gender] [bit] NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Servicess]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Servicess](
	[ServiceID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ForType] [int] NULL,
	[Duration] [float] NULL,
	[Price] [float] NULL,
	[Descriptions] [varchar](max) NULL,
	[IsDelete] [bit] NULL,
	[Images] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffID] [int] IDENTITY(1,1) NOT NULL,
	[StaffName] [varchar](50) NULL,
	[Image] [varchar](10) NULL,
	[IsAvailable] [bit] NULL,
	[IsDelete] [bit] NULL,
	[Gender] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Staff_Service_Detail]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff_Service_Detail](
	[StaffID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceID] [bigint] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Type]    Script Date: 11/11/2019 3:28:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Accessory_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[Accessory] CHECK CONSTRAINT [FK_Accessory_Category]
GO
ALTER TABLE [dbo].[Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Accessory_Type] FOREIGN KEY([ForType])
REFERENCES [dbo].[Type] ([TypeID])
GO
ALTER TABLE [dbo].[Accessory] CHECK CONSTRAINT [FK_Accessory_Type]
GO
ALTER TABLE [dbo].[Invoice_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Accessory_Account] FOREIGN KEY([BuyerUserName])
REFERENCES [dbo].[Account] ([UserName])
GO
ALTER TABLE [dbo].[Invoice_Accessory] CHECK CONSTRAINT [FK_Invoice_Accessory_Account]
GO
ALTER TABLE [dbo].[Invoice_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Accessory_Account1] FOREIGN KEY([AdminConfirm])
REFERENCES [dbo].[Account] ([UserName])
GO
ALTER TABLE [dbo].[Invoice_Accessory] CHECK CONSTRAINT [FK_Invoice_Accessory_Account1]
GO
ALTER TABLE [dbo].[Invoice_Accessory_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Accessory_Detail_Accessory] FOREIGN KEY([AccessoryID])
REFERENCES [dbo].[Accessory] ([AccessoryID])
GO
ALTER TABLE [dbo].[Invoice_Accessory_Detail] CHECK CONSTRAINT [FK_Invoice_Accessory_Detail_Accessory]
GO
ALTER TABLE [dbo].[Invoice_Accessory_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Accessory_Detail_Invoice_Accessory] FOREIGN KEY([InvoiceID])
REFERENCES [dbo].[Invoice_Accessory] ([InvoiceID])
GO
ALTER TABLE [dbo].[Invoice_Accessory_Detail] CHECK CONSTRAINT [FK_Invoice_Accessory_Detail_Invoice_Accessory]
GO
ALTER TABLE [dbo].[Invoice_Service]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Account] FOREIGN KEY([AdminConfirm])
REFERENCES [dbo].[Account] ([UserName])
GO
ALTER TABLE [dbo].[Invoice_Service] CHECK CONSTRAINT [FK_Invoice_Service_Account]
GO
ALTER TABLE [dbo].[Invoice_Service]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Pet] FOREIGN KEY([PetID])
REFERENCES [dbo].[Pet] ([PetID])
GO
ALTER TABLE [dbo].[Invoice_Service] CHECK CONSTRAINT [FK_Invoice_Service_Pet]
GO
ALTER TABLE [dbo].[Invoice_Service]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Service] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[Servicess] ([ServiceID])
GO
ALTER TABLE [dbo].[Invoice_Service] CHECK CONSTRAINT [FK_Invoice_Service_Service]
GO
ALTER TABLE [dbo].[Invoice_Service]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Staff] FOREIGN KEY([StaffDoing])
REFERENCES [dbo].[Staff] ([StaffID])
GO
ALTER TABLE [dbo].[Invoice_Service] CHECK CONSTRAINT [FK_Invoice_Service_Staff]
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD  CONSTRAINT [FK_Pet_Type] FOREIGN KEY([TypeID])
REFERENCES [dbo].[Type] ([TypeID])
GO
ALTER TABLE [dbo].[Pet] CHECK CONSTRAINT [FK_Pet_Type]
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD  CONSTRAINT [FK_Pet_User] FOREIGN KEY([OwnID])
REFERENCES [dbo].[Account] ([UserName])
GO
ALTER TABLE [dbo].[Pet] CHECK CONSTRAINT [FK_Pet_User]
GO
ALTER TABLE [dbo].[Staff_Service_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Service_Detail_Service] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[Servicess] ([ServiceID])
GO
ALTER TABLE [dbo].[Staff_Service_Detail] CHECK CONSTRAINT [FK_Staff_Service_Detail_Service]
GO
ALTER TABLE [dbo].[Staff_Service_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Service_Detail_Staff] FOREIGN KEY([StaffID])
REFERENCES [dbo].[Staff] ([StaffID])
GO
ALTER TABLE [dbo].[Staff_Service_Detail] CHECK CONSTRAINT [FK_Staff_Service_Detail_Staff]
GO
USE [master]
GO
ALTER DATABASE [PRJ321_Project_Demo1] SET  READ_WRITE 
GO
